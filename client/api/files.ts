import axios from "@/core/axios";
//вытаскиваем шаблон данных по файлу
import {fileItem} from "@/api/dto/files.dto";

//используемые типы файлов все, только фотографии или помеченные как удаленные
type FileType = "all" | "photos" | "trash";

//функция запроса на плучение с сервера всех файлов авторизованного пользователя
export const getAll = async(type: FileType = "all"): Promise<fileItem[]> => {
    return (await axios.get("/files?type=" + type)).data;
}

//функция запроса для пометки выбранного файла в виде удаленного
export const remove = (ids: String[]): Promise<void> => {
   return axios.delete("/files?ids=" + ids);
};

//функция загрузки файла с диска и добавления его на сервер
//(это все делает авторизованный пользователь)
export const uploadFile = async (options: any) => {
    const { onSuccess, onError, file, onProgress} = options;

    const formData = new FormData();
    formData.append("file", file);

    const config = {
        headers: {"Content-Type": "multipart/form-data"},
        
        //отслеживание процесса загрузки файла с диска
        onProgress: (event: ProgressEvent) => {
            onProgress({percent: (event.loaded / event.total)*100})
        }
    };

    try {
        //осуществление запроса добавление на сервер файла, загруженного с диска
        const {data} = await axios.post("files", formData, config);

        //указанный запрос осуществился успешно
        onSuccess();

        return data;
    } 
    catch (err)
    {
        //указанный запрос не прошел
        onError({err});
    }
}
