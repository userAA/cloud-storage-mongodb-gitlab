//интерфейс данных для авторизации зарегистрированного пользователя
export interface LoginFormDTO {
    email: string;
    password: string;
}

//интерфейс данных о результате авторизации зарегистрированного пользователя
export interface LoginResponseDTO {
    token: string;
}

//интерфейс данных для регистрации пользователя
export type RegisterFormDTO = LoginFormDTO & {fullName: string};

//интерфейс данных о результате регистрации пользователя
export type RegisterResponseDTO = LoginResponseDTO;

//интерфейс данных о зарегистрированном и авторизованном пользователе
export interface User {
    _id: string;
    email: string;
    fullName: string;
}