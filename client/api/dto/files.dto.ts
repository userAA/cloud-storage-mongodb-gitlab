import {User} from "@/api/dto/auth.dto";

//шаблон полной информации о файле
export interface fileItem {
    fileName: string;
    originalName: string;
    size: number;
    mimetype: string;
    user: User;
    deleteAt: string | null;
    _id: string;
}