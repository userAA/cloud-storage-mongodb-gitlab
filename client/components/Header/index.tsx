import React from "react";
import {Layout, Avatar, Menu, Popover, Button} from "antd";
import styles from "./Header.module.scss";
import {CloudOutlined} from "@ant-design/icons";
//вытаскиваем хук useRouter
import { useRouter } from "next/router";

import * as Api from "@/api";

export const Header: React.FC = () => {
    //определяем маршрутизатор с помощью хука useRouter
    const router = useRouter();
    //определяем роутер выбранной клавиши заглавного меню
    const selectedMenu = router.pathname;

    //обработчик события снятия авторизации с автризованного пользователя
    const onClickLogout = () => {
        if (window.confirm("Do you really want to get out?"))
        {
            //осуществляем указанное событие
            Api.auth.logout();
            //переходим на страницу авторизации пользователя
            location.href = "/dashboard/auth";
        }
    }

    return (
        <Layout.Header className={styles.root}>
            <div className={styles.headerInner}>
                <div className={styles.headerLeft}>
                    <h2>
                        <CloudOutlined />
                        Cloud Storage
                    </h2>
                
                    <Menu
                        className={styles.topMenu}
                        theme="dark"
                        mode="horizontal"
                        defaultSelectedKeys={[selectedMenu]}
                        onSelect = {({key}) => router.push(key)}
                        items = {[
                            //клавиша перехода на главную страницу проекта
                            { key: "/dashboard", label: "Main"},
                            //клавиша перехода на страницу показа информации об авторизованном пользователе
                            { key: "/dashboard/profile", label: "Profile"}
                        ]}
                    />
                </div>

                {/*компонент снятия авторизации с авторизованного пользователя */}
                <div className={styles.headerRight}>
                    <Popover
                        trigger="click"
                        content = {
                            //кнопка снятия авторизации с авторизованного пользователя
                            <Button onClick={onClickLogout} type="primary" danger>
                                Escape
                            </Button>
                        }
                    >
                        <Avatar>A</Avatar>
                    </Popover>
                </div>
            </div>
        </Layout.Header>
    )
}