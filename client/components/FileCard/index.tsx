import React from "react"
import styles from "./FileCard.module.scss"
import {getExtensionFromFileName} from "@/utils/getExtensionFromFileName" 
import { getColorByExtension} from "@/utils/getColorByExtension"
import {FileTextOutlined} from "@ant-design/icons"

//шаблон данных по каждому файлу авторизованного пользователя
interface FileCardProps {
    //имя файла на сервере
    fileName: string;
    //первоначальное имя файла
    originalName: string;
}

//компонент показа любогого файла автоизованного пользователя
export const FileCard: React.FC<FileCardProps> = ({originalName, fileName}) => {
    //определяем расширение каждого файла авторизованного пользователя
    const ext = getExtensionFromFileName(fileName);
    
    //определяем является ли некоторый файл авторизованного пользователя изображением, если да то определяем его url
    const imageUrl = ext && ['jpg', 'jpeg', 'png', 'bmp'].includes(ext) ? "http://localhost:7777/uploads/" + fileName : "";

    const color = getColorByExtension(ext);
    
    //определяем цвет, которым отметим расширение файла-изображения авторизованного пользователя
    const classColor = styles[color];

    return (
        <div className={styles.root}>
            <div className={styles.icon}>
                <i className={classColor}>{ext}</i>
                { ['jpg', 'jpeg', 'png', 'bmp'].includes(ext) ? (
                    //показываем файл-изображение авторизованного пользователя
                    <img className={styles.image} src={imageUrl} alt="File" />
                ) : (
                    //шаблон показа текстового файла авторизованного пользователя
                    <FileTextOutlined/>
                )}
            </div>
            {/*Выводим первоначальное имя каждого файла авторизованного пользователя */}
            <span>{originalName}</span>
        </div>
    )
}