import { Extension } from "./getColorByExtension";

//функция определения расширения файла
export const getExtensionFromFileName = (fileName: string) => {
    return fileName.split(".").pop() as Extension;
}