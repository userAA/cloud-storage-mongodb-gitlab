import {GetServerSidePropsContext, NextPage} from "next";
//импортируем функцию проверки существования авторизованного пользователя
import { checkAuth } from "@/utils/checkAuth";
import React from "react";
//импотируем полный шаблон web-страницы
import { Layout } from "../../layouts/Layout";

import * as Api from "@/api";
//вытаскиваем шаблон данных по файлам
import {fileItem} from "@/api/dto/files.dto";
//импортируем компонент операций с файлами авторизованного пользователя
import { DashboardLayout } from "@/layouts/DashboardLayout";
//вытаскиваем компонент показа файлов авторизованного пользователя
import { Files } from "@/modules/Files";

//формируем шаблон props страницы файлов, удаленных авторизованным пользователем
interface Props {
    items: fileItem[];
}

//шаблон страницы показа файлов, удаленных авторизованным пользователем
const DashboardTrash: NextPage<Props> = ({items}) => {
    return  (
        //компонент операций с файлами авторизованного пользователя
        <DashboardLayout>
            {/*Компонент файлов, удаленных авторизованным пользователем*/}
            <Files items={items} />
        </DashboardLayout>
    )
}

DashboardTrash.getLayout = (page: React.ReactNode) => {
    //задаем заголовок страницы показа файлов, удаленных авторизованным пользователем
    return <Layout title="Dashboard / Trash">{page}</Layout>
}

//функция получения props по странице показа файлов, удаленных авторизованным пользователем
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //проверяем существует ли авторизованный пользователь
    const authProps = await checkAuth(ctx);

    //если нет то производим перенаправление на страницу авторизации зарегистированного пользователя 
    if ('redirect' in authProps) {
        return authProps;
    }

    try 
    {
        //получаем все файлы, удаленные авторизованным пользователем
        const items = await Api.files.getAll("trash");

        return {
            //формируем искомые props
            props: {
                items
            }
        }
    }
    catch (err) {
        console.log(err);
        //в случае неудачного запроса 
        return {
            props: {items : []}
        }
    }
}

export default DashboardTrash;