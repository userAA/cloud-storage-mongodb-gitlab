import {GetServerSidePropsContext, NextPage} from "next";
//импортируем функцию проверки существования авторизованного пользователя
import { checkAuth } from "@/utils/checkAuth";
import React from "react";
//импотируем полный шаблон web-страницы
import { Layout } from "../../layouts/Layout";
//импортируем компонент операций с файлами авторизованного пользователя
import { DashboardLayout } from "../../layouts/DashboardLayout";

import * as Api from "@/api";
//вытаскиваем шаблон данных по файлам
import {fileItem} from "@/api/dto/files.dto";
//вытаскиваем компонент показа файлов авторизованного пользователя
import { Files } from "@/modules/Files";

//формируем шаблон props страницы показа фотографий авторизованного пользователя
interface Props {
    items: fileItem[];
}

//шаблон страницы показа фотографий авторизованного пользователя
const DashboardPhotos: NextPage<Props> = ({items}) => {
    return  (
        //компонент операций с файлами авторизованного пользователя
        <DashboardLayout>
            {/*Компонент показа фотографий авторизованного пользователя */}
            <Files items={items} withActions/>
        </DashboardLayout>
    )
}

DashboardPhotos.getLayout = (page: React.ReactNode) => {
    //задаем заголовок страницы показа фотографий авторизованного пользователя
    return <Layout title="Dashboard / Фото">{page}</Layout>
}

//функция получения props по странице показа фотографий авторизованного пользователя
export const getServerSideProps = async (ctx: GetServerSidePropsContext) => {
    //проверяем существует ли авторизованный пользователь
    const authProps = await checkAuth(ctx);

    //если нет то производим перенаправление на страницу авторизации зарегистированного пользователя 
    if ('redirect' in authProps) {
        return authProps;
    }

    try 
    {
        //получаем все фотографии авторизованного пользователя
        const items = await Api.files.getAll("photos");

        return {
            //формируем искомые props
            props: {
                items
            }
        }
    }
    catch (err) {
        console.log(err);
        //в случае неудачного запроса 
        return {
            props: {items : []}
        }
    }
}

export default DashboardPhotos;